# PHP build environment

This repository contains several Docker images to build PHP applications and includes the most popular extensions already included.

## Included extensions

In this image the following extensions are included:

 - APCu
 - bcmath
 - GD
 - IMAP (with SSL support)
 - intl
 - LDAP
 - PDO MySQL
 - PDO PgSQL
 - SOAP
 - ZIP
 - Composer 2
 
The PHP default configuration `php.ini-production` is activated.
 
## Additional extensions in development images

The images ending in `-dev` are development images, those have:

 - xDebug 3 installed
 - the PHP default configuration `php.ini-development` activated
 - Chromium and and Chromium-Chromedriver installed

## Different versions

Different Docker images are offered for each major PHP version. These images mainly differ in the PHP base image used. This can drastically reduce build time. The following images are available:

| Image              | Base               | Status  |
|--------------------|--------------------|---------|
| 8.3-alpine         | php:8.3-alpine     | ✅      |
| 8.3-alpine-dev     | php:8.3-alpine     | ✅      |
| 8.3-fpm-alpine     | php:8.3-fpm-alpine | ✅      |
| 8.3-fpm-alpine-dev | php:8.3-fpm-alpine | ✅      |
| 8.2-alpine         | php:8.2-alpine     | ✅      |
| 8.2-alpine-dev     | php:8.2-alpine     | ✅      |
| 8.2-fpm-alpine     | php:8.2-fpm-alpine | ✅      |
| 8.2-fpm-alpine-dev | php:8.2-fpm-alpine | ✅      |
| 8.1-alpine         | php:8.1-alpine     | ✅      |
| 8.1-alpine-dev     | php:8.1-alpine     | ✅      |
| 8.1-fpm-alpine     | php:8.1-fpm-alpine | ✅      |
| 8.1-fpm-alpine-dev | php:8.1-fpm-alpine | ✅      |
| 8.0-alpine         | php:8.0-alpine     | ❌      |
| 8.0-alpine-dev     | php:8.0-alpine     | ❌      |
| 8.0-fpm-alpine     | php:8.0-fpm-alpine | ❌      |
| 8.0-fpm-alpine-dev | php:8.0-fpm-alpine | ❌      |
| 7.4-alpine         | php:7.4-alpine     | ❌      |
| 7.4-alpine-dev     | php:7.4-alpine     | ❌      |
| 7.4-fpm-alpine     | php:7.4-fpm-alpine | ❌      |
| 7.4-fpm-alpine-dev | php:7.4-fpm-alpine | ❌      |
| 7.3-alpine         | php:7.3-alpine     | ❌      |
| 7.3-alpine-dev     | php:7.3-alpine     | ❌      |
| 7.3-fpm-alpine     | php:7.3-fpm-alpine | ❌      |
| 7.3-fpm-alpine-dev | php:7.3-fpm-alpine | ❌      |
